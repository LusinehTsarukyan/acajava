package lesson0;

public class _2_Literals {
   public static void main(String[] args) {



      boolean result = false;


      byte b = 100;

      short s = 10000;

      int i = 100000;
      long l = 100000_000;
      int decVal = 26;              // The number 26, in decimal
      int hexVal = 0x1a;            // The number 26, in hexadecimal
      int binVal = 0b1010;         // The number 26, in binary

      float f1 = 122.0f;

      double d3 = 111;


      double d1 = 123.4;
      double d2 = 1.234e2;          // same value as d1, but in scientific notation

      char c1 = '\u0108';
      char c2 = 'C';

      String s1 = "some string literal";
      int[] a1 = new int[42]; // also an object

      int[][] a2 = {
         {1, 2, 3},
         {4, 5, 6}
      };

   }
}
